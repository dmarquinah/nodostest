var dataJSON;
var arrayPolilineas = [];
$(document).ready(function(){
    // $('#formSearch').submit(function(e){
    //   e.preventDefault();
    // });
    // var map;
    // map = new google.maps.Map(document.getElementById('mapDiv'),{
    //   center: {
    //     lat: -9.4597646,
    //     lng: -76.1849931
    //   },
    //   zoom: 5.75
    // });
    // $.get("/javascripts/data.JSON", function(data){
    //   var circuitos = [];
    //   dataJSON = data;
    //   for (var i in data){
    //     circuitos.push(data[i]["circuito_padre"]);
    //   }

    //   //Variable circuitos se refiere al conjunto de circuitos en el data.JSON
    //   //Elimina duplicados
    //   circuitos = circuitos.filter((v,i) => circuitos.indexOf(v) === i);
      
    //   for (i in circuitos){
    //     var poliLinea;
    //     var arrayCircuito = [];
    //     for(var j in data){
    //       var iterationObj = {};
    //       if (circuitos[i] === data[j]['circuito_padre']){
    //         iterationObj['lat'] = data[j]['lat'];
    //         iterationObj['lng'] = data[j]['lng'];
    //         iterationObj['id'] = data[j]['id'];
    //         iterationObj["circuito_padre"] = data[j]["circuito_padre"];
    //         arrayCircuito.push(iterationObj);
    //       }
    //     }
        
    //     poliLinea = new google.maps.Polyline({
    //       path : arrayCircuito,
    //       geodesic: true,
    //       strokeColor: '#152e6d',
    //       strokeOpacity: 0.1,
    //       strokeWeight: 5
    //     });
    //     arrayPolilineas.push([poliLinea,circuitos[i]]);
    //     setEvents(poliLinea);
    //   }

    //   function setEvents(polyLine){
    //     var circuit;
    //     google.maps.event.addListener(polyLine, 'click', (e) => {
    //       var latIni = polyLine.getPath().b[0].lat();
    //       var lngIni = polyLine.getPath().b[0].lng();
    //       for(i in data){
    //         var error = Math.abs(data[i].lng - lngIni);
    //         if(data[i].lat === latIni && error <= 0.001){
    //           circuit = data[i].circuito_padre;
    //           break;
    //         }
    //       }
    //       var info = `<table class='table'><thead><tr><th scope='col'>ID</th><th scope='col'>Ciudad</th><th scope='col'>Latitud</th><th scope='col'>Longitud</th></thead><tbody>`;
    //       for(i in data){
    //         if(data[i].circuito_padre === circuit){
    //           info+= `<tr><th scope='row'>${data[i].id}</th><td>${data[i].nombreNodo}</td><td>${data[i].lat}</td><td>${data[i].lng}</td></tr>`;
    //         }
    //       }
    //       info += '</tbody></table>';

    //       $('#tituloCircuito').html(`Circuito ${circuit}`);
    //       $('#tableExample').html(info);

    //       for(var i in arrayPolilineas){
    //         arrayPolilineas[i][0].setOptions({strokeOpacity: 0.1});
    //       }

    //       if(polyLine.strokeOpacity === 1){
    //         polyLine.setOptions({strokeOpacity: 0.1});
    //       }else{
    //         polyLine.setOptions({strokeOpacity: 1});
    //       }
          
    //     });
    //     polyLine.setMap(map);
    //   }

    //   var marker,i;
    //   var infoWindow = new google.maps.InfoWindow;
    //   var icon = {
    //     url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png", // url
    //     scaledSize: new google.maps.Size(30, 30), // scaled size
    //     origin: new google.maps.Point(0,0), // origin
    //     anchor: new google.maps.Point(0, 0) // anchor
    // };
    //   //console.log(dataJSON);
    //   for(i in data){
    //     marker = new google.maps.Marker({
    //       position : new google.maps.LatLng(data[i]['lat'],data[i]['lng']),
    //       map : map,
    //       icon: icon
    //     });
    //     setMarkerEvent(marker);
    //   }
      
    //   function setMarkerEvent(){
    //     google.maps.event.addListener(marker, 'click', (function(marker, i){
    //       return function(){
    //         infoWindow.setContent(`${data[i]['nombreNodo']}`);
    //         infoWindow.open(map, marker);
    //       }
    //     })(marker,i));
    //   }
    // });
    var arr = [[30,'CR 77340'],[26,'CR 77341'],[0,'CR 77342'],[18,'CR 77342'],[20,'CR 77343'],[15,'CR 77344'],[17,'CR 77345'],[14,'CR 77346'],[13,'CR 77347'],[12,'CR 77348'],[16,'CR 77349'],[31,'CR 77350'],[29,'CR 77351'],[25,'CR 77352'],[21,'CR 77353'],[24,'CR 77354'],[23,'CR 77355'],[16,'CR 77356'],[12,'CR 77357'],[27,'CR 77358'],[11,'CR 77391'],[6,'CR 77392'],[6,'CR 77393'],[3,'CR 77394'],[3,'CR 77395'],[3,'CR 77396'],[3,'CR 77397'],[5,'CR 77398'],[5,'CR 77399'],[5,'CR 77400'],[4,'CR 77401'],[1,'CR 77402'],[2,'CR 77403'],[10,'CR 77404'],[22,'CR 77405'],[7,'CR 77406'],[8,'CR 77407'],[9,'CR 77408'],[28,'CR 77409'],[14,'CR 77410'],[6,'CR 77411'],[6,'CR 77412'],[3,'CR 77413'],[3,'CR 77414'],[3,'CR 77415'],[3,'CR 77416'],[5,'CR 77417'],[5,'CR 77418'],[5,'CR 77419'],[10,'CR 77420'],[1,'CR 77421'],[0,'CR 77422'],[7,'CR 77424'],[7,'CR 77425'],[8,'CR 77426'],[8,'CR 77427'],[2,'CR 77428']]



    $.get('/javascripts/data2.JSON', function(data){
      for (i in arr){
        for(j in data.circuitos){
          if (data.circuitos[j]['codCircuito'] == arr[i][1]){
            data.circuitos[j]['orden'] = arr[i][0];
            break;
          }
        }
      }

      //console.log(JSON.stringify(data));

    });


  });

$('#btnSearch').on('click', buscarInput);

function buscarInput(){
  var encontrado = false;
  var val = $('#searchInput').val();
  for(var i in dataJSON){
    if(dataJSON[i].nombreNodo === val){
      var circuito = dataJSON[i].circuito_padre;
      encontrado = true;
      break;
    }
  }

  if(encontrado){
    for(i in arrayPolilineas){
      if (arrayPolilineas[i][1] === circuito){
        google.maps.event.trigger(arrayPolilineas[i][0],'click',{});
        break;
      }
    }

  }else{
    return (function(){
      $('#tituloCircuito').html('No hay coincidencias en la búsqueda');
      $('#tableExample').html('');
    })();
  }
  
}
