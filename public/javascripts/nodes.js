$('#dropdownBtn button').on('click', function(e){
  $('#divBusqueda [id^=div]').hide();
  $('#'+this.value).show();
  $('#inputGeneral').val('');
});

var dataJSON;

$(document).ready(function(){
  $.get('/javascripts/data.JSON', function(data){
    drawMap(data);
    dataJSON = data;
    var infoLocalidades = data.localidades.sort();
    for (var i in infoLocalidades){
      var item = infoLocalidades[i];
      $('#select1').append($('<option>', {
        value: item,
        text: item
      }));

      $('#select2').append($('<option>', {
        value: item,
        text: item
      }));
    }
  });
});

$('#buscarInput').on('click',function(e){
  var value = $('#inputGeneral').val();
  value = value.replace(/ /g, '');
  var values = value.split("-");
  getRoutesTable(values[0].toUpperCase(),values[1].toUpperCase());
});

$('#buscarSelect').on('click',function(e){
  var value1 = $('#select1 option:selected').val();
  var value2 = $('#select2 option:selected').val();
  getRoutesTable(value1.toUpperCase(), value2.toUpperCase());
});

function getRoutesTable(locInicio, locFin){
  var circuitosEncontrados = [];
  console.log(circuitosEncontrados);
  var indexInicio;
  var indexFin;
  for(var i in dataJSON.circuitos){
    indexInicio = dataJSON['circuitos'][i]['circuito'].indexOf(locInicio);
    if(indexInicio >= 0){
      indexFin = dataJSON['circuitos'][i]['circuito'].indexOf(locFin);
      if( indexFin >= 0 && indexFin-indexInicio == 1){
        // console.log('index Inicio: ', indexInicio);
        // console.log('index Fin: ', indexFin);
        circuitosEncontrados.push(dataJSON.circuitos[i]);
      }
    }
  }

  if(circuitosEncontrados.length){
    var info = `<table class='table'><thead><tr><th scope='col'>#</th><th scope='col'>cod. Circuito</th><th scope='col'>Circuito</th><th scope='col'>Ruta</th></thead><tbody>`;
    var circuito = [];
    for(var j in circuitosEncontrados){
      info += `<tr><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
      circuito.push(circuitosEncontrados[j]['circuito']);
    }
    info += '</tbody></table>';

    $('#infoTabla').html(info);
    for(var j in circuitosEncontrados){
      setEvent(circuitosEncontrados[j]['circuito'], j);
    }


  }else{
    $('#infoTabla').html("<h2>No se han encontrado coincidencias</h2>");
  }

  function setEvent(ruta, i){
    $("#ruta" + i).on('click', function(e){
      $('#infoRuta').html(ruta.join(" - "));
    });
  }
}

function drawMap(data){
  var map;
  map = new google.maps.Map(document.getElementById('divVisual'),{
    center: {
      lat: -9.4597646,
      lng: -76.1849931
    },
    zoom: 5.75
  });

  var marker,i;
  var infoWindow = new google.maps.InfoWindow;
  var icon = {
    url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png", // url
    scaledSize: new google.maps.Size(30, 30), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0) // anchor
  };

  for (var i=0;i<data.localidades.length-83;i++){

    var localidad = data.localidades[i];
    marker = new google.maps.Marker({
      position : new google.maps.LatLng(localidad[1],localidad[2]),
      map : map,
      icon: icon
    });
    setMarkerEvent(marker, localidad[0]);
    
  }

  function setMarkerEvent(marker, localidad){
    google.maps.event.addListener(marker, 'click', (function(marker){
      return function(){
        infoWindow.setContent(localidad);
        infoWindow.open(map, marker);
      }
    })(marker));
  }

}