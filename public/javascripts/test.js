var points=[];
var circuito = [];
var listaCirc = [];
var listaTextos = [];
var n;
var dataJSON;

(function() {
  //La variable canvas es global y se puede llamar desde cualquier lugar para utilizarlo.
  var canvas = this.__canvas = new fabric.Canvas('c', { selection: false, controlsAboveOverlay : true });
    canvas.setHeight(window.innerHeight);
    canvas.setWidth($('body').width());
    //Set del zoom inicial
    canvas.setZoom(0.8);
    delta = new fabric.Point(210, -30);
    //Se setea el punto inicial de visualización en el punto delta = (210, -30)
    canvas.relativePan(delta);
    fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
    $('#btnBuscar').click((e)=> {
      //e.preventDefault impide que al hacer click en el botón se vuelva a cargar la página
      e.preventDefault();
      //Función getCircuito muestra la información buscada mediante la barra superior de navegación.
      getCircuito($('#inputCR').val());
    });
    
    function makeCircle(left, top, name, size, arrayLineasAsociadas) {
      //Los circulos son creados dinámicamente. Se utiliza el tercer parámetro de la propiedad "ubicaciones" del JSON principal para determinar el tamaño del circulo.
      var c = new fabric.Circle({
        left: left,
        top: top,
        strokeWidth: 3-size,
        radius: 6-size,
        fill: '#fff',
        stroke: '#666',
        name: name
      });
      c.hasControls = c.hasBorders = false;
      //Se asigna propiedades line que indica la unión de puntos que presenta el nodo: Ejemplo Puno - Desaguadero. Puede tener más de 2
      for (var i in arrayLineasAsociadas){
        c[`line${i}`] = arrayLineasAsociadas[i];
      }
      //Se guardan los círculos dentro de un array global para su acceso y modificación en cualquier parte del código.
      listaCirc.push(c);
      return c;
    }

    function makeText(left, top, text, size){
      //Para cada nodo se genera el texto que lo representa (el nombre del nodo)
      if(text.indexOf(' ')>=0){
        text = text.replace(' ','\n');
      }
      var t = new fabric.Text(text, {
        fontSize: 12-size,
        left: left,
        top: top-20+size,
        fontFamily: 'Verdana',
        selectable: false,
        evented: false
      });
      //Se guarda en un array global todos los textos
      listaTextos.push(t);
      return t;
    }
  
    function makeLine(coords, tipoRed ,locInicio, locFin) {
      var color;
      //Se dibujan las líneas entre nodos
      switch (tipoRed){
        case "TDP":
          color = "#17375e";
          break;
        case "CLARO":
          color = "#ff0000";
          break;
        case "TERCEROS":
          color = "#558ed5";
          break;
        default:
          color = "#000000";
          break;
      }

      return new fabric.Line(coords, {
        fill: color,
        stroke: color,
        strokeWidth: 3,
        selectable: false,
        evented: true,
        locInicio: locInicio,
        locFin: locFin,
        red: tipoRed,
        perPixelTargetFind: true,
        hoverCursor: "pointer"
      });
    }
    
    var panning = false;
    canvas.on('mouse:up', function(e){
        //Cuando el botón del mouse se levanta, la propiedad panning se asigna falso y el canvas ya no se puede mover.
        panning = false;
        if (e.target){
          console.log(e.target);
        }
    });

    canvas.on('mouse:down', function(e){
      //Cuando el botón del mouse se presiona, la propiedad panning se asinga verdadero lo que permite mover el canvas.
      if (e.target == null){
        panning = true;
      }else{
        
        if (e.target.red != null){
          //Tabla al hacer click en una linea
          getRoutesTable(e.target.locInicio, e.target.locFin);
        } else {
          //Info al hacer clic en un circulo
          getInfoNodo(e.target);
        }
        
      }
  });   
    canvas.on('mouse:move', function(e){
      //Al mover el mouse mientras se tiene el botón del mouse presionado, el canvas se mueve
      if(panning && e && e.e){
        var delta =  new fabric.Point(e.e.movementX, e.e.movementY);
        canvas.relativePan(delta);
      }
    }); 


    canvas.on('mouse:wheel', function(opt) {
      //Al usar la rueda del mouse se hace un zoom al canvas
      var delta = opt.e.deltaY;
      var pointer = canvas.getPointer(opt.e);
      
      var zoom = canvas.getZoom();
      zoom = zoom - delta/500;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.5) zoom = 0.5;

      canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
      opt.e.preventDefault();
      opt.e.stopPropagation();
    });

    //Debug
    canvas.on('object:moving', function(opt){
        var p = opt.target;
        var objProp = Object.keys(p);
        for(var i in objProp){
          if(objProp[i].indexOf('line') >= 0){
            if(p.name == p[objProp[i]]['locInicio']){
              p[objProp[i]] && p[objProp[i]].set({'x1':p.left, 'y1':p.top});
            }else if(p.name == p[objProp[i]]['locFin']){
              p[objProp[i]] && p[objProp[i]].set({'x2':p.left, 'y2':p.top});
            }
          }
        }
        canvas.renderAll();
    });
  

    var arrayLineas = [];
    var arrayEnlaces = [];

    //Jquery.get se utiliza para jalar la información del JSON al iniciar la web
    jQuery.get('/javascripts/DataSetCR.json',(data)=> {   
      dataJSON = data;   
      var circ;
      var circNombre;
      var locInicio;
      var locFinal;
      var circuitos = data.circuitos;
      for (var i = 0; i< circuitos.length; i++){
        locInicio = circuitos[i][0];
        locFinal = circuitos[i][1];
        tipoRed = circuitos[i][2];
        crearLineas(data.ubicaciones[locInicio], data.ubicaciones[locFinal], tipoRed ,locInicio, locFinal);
      }

      //En arrayLineas se guardan todas las lineas (conexiones) existentes
      var lineaRef;
      
      for (var i in data.ubicaciones){
        //cada localidad (circulo) tendrá ciertas líneas asignadas
        lineaRef = [];
        for (var j in arrayLineas){
          if (arrayLineas[j]['locInicio'] == i || arrayLineas[j]['locFin'] == i){
            lineaRef.push(arrayLineas[j]);  
          }
        }
        if (i.indexOf('Aux.') == -1){
          //Los nodos que tienen 'Aux.' en su nombre se han creado para poder dibujar correctamente el mapa, pero no debería de mostrarse
          crearCirculos(data.ubicaciones[i], i, lineaRef, data.ubicaciones[i][2]);
        }
        
      }
      canvas.renderAll();
      
    });

    function crearCirculos(dataArray , nombreLinea, lineasAsociadas, sizeCircle){
      canvas.add(makeCircle(dataArray[0], dataArray[1], nombreLinea, sizeCircle, lineasAsociadas));
      canvas.add(makeText(dataArray[0], dataArray[1], nombreLinea, sizeCircle+1));
    }
    
    function crearLineas(arrayInicio, arrayFin, tipoRed, localidadInicio, localidadFinal){
      var len = arrayEnlaces.length;
      arrayEnlaces.push(`${localidadInicio}-${localidadFinal}`);
      arrayEnlaces = [...new Set(arrayEnlaces)]; //Eliminar duplicados
      var linea = makeLine([arrayInicio[0],arrayInicio[1],arrayFin[0],arrayFin[1]], tipoRed ,localidadInicio, localidadFinal);
      if(len != arrayEnlaces.length){
        arrayLineas.push(linea);
        canvas.add(linea);
      } 
    }

    function getAllIndexes(arr, val) {
      var indexes = [], i = -1;
      while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
      }
      return indexes;
    }

    function getRoutesTable(locInicio, locFin){
      let circuitosEncontrados = [];
      let indexInicio;
      let indexFin;
      for(var i in dataJSON.CRS){
        indexInicio = dataJSON['CRS'][i]['circuito'].indexOf(locInicio);
        if( indexInicio >= 0){
          //Busca si hay nodos repetidos en la red, indexesI e indexesF tienen los índices en donde se repiten algunos nodos
          let indexesI = getAllIndexes(dataJSON['CRS'][i]['circuito'], locInicio);
          let indexesF = getAllIndexes(dataJSON['CRS'][i]['circuito'], locFin);
          //Si solo se repite una vez cada inicio y final se despliegan normalmente los circuitos
          if (indexesF.length == 1 && indexesI.length == 1){
            indexFin = dataJSON['CRS'][i]['circuito'].indexOf(locFin);
            if( indexFin >= 0 && Math.abs(indexFin - indexInicio) == 1){
              circuitosEncontrados.push(dataJSON.CRS[i]);
            }
            
          }else{
            //Si se repite más de una vez, se hace la comparación de las conexiones con cada nodo, para validar que lo buscado existe
            //Ejem
            //CIRCUITO 1 : [a, b, c, a, d]
            //Al buscar la conexión "a - d", si no se hace esta validación, este circuito (CIRCUITO 1) no se tomará en cuenta (si debería tomarse en cuenta), pues solo se validaría
            //con la primera coincidencia que se encuentra, en este caso: ("a - b").
            let indexp = indexesF.length == 1 ? indexesF[0] : indexesI[0];
            let indexes = indexesF.length > indexesI.length ? indexesF : indexesI;
            for (let j = 0; j< indexes.length;j++){
              if (Math.abs(indexp - indexes[j]) == 1){
                circuitosEncontrados.push(dataJSON.CRS[i]);
              }
            }
          }
        }
      }
      //Ordena los circuitos en función a la propiedad BW (ancho de banda)
      circuitosEncontrados.sort((a,b) => {
        return a.BW - b.BW;
      });
      //Se invierte el array para mostrar la tabla de circuitos del más al menos importante.
      circuitosEncontrados.reverse();

      if(circuitosEncontrados.length){
        let info = ``;
        var circuito = [];
        for(let j in circuitosEncontrados){
          //Se ha designado arbitrariamente mostrar colores en función a su ancho de banda, para hacer esta separación se tomaron todos los BW de mayor a menor y se separaron en 3 grupos iguales.
          
          if (circuitosEncontrados[j]['BW'] > 4300){
            info += `<tr class='table-success' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          } else if (circuitosEncontrados[j]['BW'] > 1100){
            info += `<tr class='table-warning' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          } else {
            info += `<tr class='table-danger' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          }
          circuito.push(circuitosEncontrados[j]['circuito']);
        }

        //Determinar la cantidad de números que tendrá el navegador de la tabla (la tabla se separa cada 5 circuitos.)
        let num;
          n = circuitosEncontrados.length;
          if (circuitosEncontrados.length % 5 != 0){
            num = Math.floor(circuitosEncontrados.length /5) + 1;
          }else {
            num = circuitosEncontrados.length / 5;
          }
        
        let pagin = ``;
          
        for (let i = 0; i < num; i++){
          pagin += `<li class='navpage page-item ${ i == 0 ? 'active' : ''}'><a class='page-link clickPage' href='#'>${ i+1 }</a></li>`;
        }

        $('#cuerpoTabla').empty();
        $('.navpage').remove();
        $('#contenido').hide('fast');
        $('#cuerpoTabla').append(info);
        $('#paginat > li:nth-child(1)').after(pagin);

        //Se reduce la tabla en grupos de 5 para mostrar la información de una mejor manera
        $('a.clickPage').click((e)=> {
          $('#paginat > li.active').toggleClass('active');
          $(e.target).parent().addClass('active');
          var numItem = $(e.target).text();
          for (let i = 1; i <= circuitosEncontrados.length; i++){
            let $row = $(`#cuerpoTabla > tr:nth-child(${i})`);
            if (i > (numItem-1)*5 && i <= numItem*5){
              $row.show();
            }else{
              $row.hide();
            } 
          }
          return false;
        });
        $('#infoAdicional').hide();

        $('#infoTramo').html(`<b>Tramo ${locInicio} - ${locFin}</b>`);
        $('#infoTramo').show('fast');
        $('#infoTabla').show('fast');
        $('#paginationDiv').show('fast');
        
        for(var j in circuitosEncontrados){
          setEvent(circuitosEncontrados[j]['circuito'], j, locInicio, locFin);
        }

        setHighlight();

      }else{
        $('#infoTramo').hide();
        $('#infoTabla').hide();
        $('#infoAdicional').html("<div class='alert alert-danger' role='alert'>No se han encontrado coincidencias</div>");
        $('#infoAdicional').show('fast');
      }

      //Las funciones que se muestran más abajo, son en base a esta función principal, solo que con pequeños ajustes para usarla con el buscador o con el nodo. 
    }

    function setHighlight(){
      //Cuando se hace click en alguna fila de la tabla, este se sombrea para tenerlo seleccionado y el usuario sepa qué es lo último que se ha seleccionado.
      $('#tableBS').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
      });
    }

    function getInfoNodo(nodo) {
      let circuitosEncontrados = [];
      for (let i in dataJSON.CRS){
        let indexI = dataJSON.CRS[i].circuito.indexOf(nodo.name);
        if (indexI >= 0) {
          circuitosEncontrados.push(dataJSON.CRS[i]);
        }
      }
        circuitosEncontrados.sort((a,b) => {
          return a.BW - b.BW;
        });
        circuitosEncontrados.reverse();

        if(circuitosEncontrados.length){
          let info = ``;
          var circuito = [];
          for(let j in circuitosEncontrados){
            if (circuitosEncontrados[j]['BW'] > 4300){
              info += `<tr class='table-success' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            } else if (circuitosEncontrados[j]['BW'] > 1100){
              info += `<tr class='table-warning' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            } else {
              info += `<tr class='table-danger' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            }
            circuito.push(circuitosEncontrados[j]['circuito']);
          }

          let num;
          n = circuitosEncontrados.length;
          if (circuitosEncontrados.length % 5 != 0){
            num = Math.floor(circuitosEncontrados.length /5) + 1;
          }else {
            num = circuitosEncontrados.length / 5;
          }

          let pagin = ``;
          
          for (let i = 0; i < num; i++){
            pagin += `<li class='navpage page-item ${ i == 0 ? 'active' : ''}'><a class='page-link clickPage' href='#'>${ i+1 }</a></li>`;
          }

          $('#cuerpoTabla').empty();
          $('.navpage').remove();
          $('#contenido').hide('fast');
          $('#cuerpoTabla').append(info);
          $('#paginat > li:nth-child(1)').after(pagin);

          $('a.clickPage').click((e)=> {
            $('#paginat > li.active').toggleClass('active');
            $(e.target).parent().addClass('active');
            var numItem = $(e.target).text();
            for (let i = 1; i <= circuitosEncontrados.length; i++){
              let $row = $(`#cuerpoTabla > tr:nth-child(${i})`);
              if (i > (numItem-1)*5 && i <= numItem*5){
                $row.show();
              }else{
                $row.hide();
              }
            }
            return false;
          });
          
          $('#infoAdicional').hide();
          $('#infoTramo').html(`<b>Nodo ${nodo.name}</b>`);
          $('#infoTramo').show('fast');
          $('#infoTabla').show('fast');
          $('#paginationDiv').show('fast');

          for(var j in circuitosEncontrados){
            setEvent(circuitosEncontrados[j]['circuito'], j, nodo.name, nodo.name);
          }
  
        }else{
          $('#infoTramo').hide();
          $('#infoTabla').hide();
          
          $('#infoAdicional').html("<div class='alert alert-danger' role='alert'>No se han encontrado coincidencias</div>");
          $('#infoAdicional').show('fast');
        }
    }

    function setEvent(ruta, i, localidadInicio, localidadFin){
      $("#ruta" + i).on('click', function(e){
        var color, zoomw, zoomh;
        for (var l in arrayLineas){
          switch (arrayLineas[l].red){
            case 'TDP':
              color = '#b6c5d8';
              break;

            case 'CLARO':
              color = "#ffe0e0";
              break;

            case 'TERCEROS':
              color = "#b6c5d8";
              break;

            default:
              break;
          }
          arrayLineas[l].set({fill:color, stroke: color});
        }

        for (var l in listaCirc){
          listaCirc[l].set({stroke : '#666'});
        }
        var cont = 0;
        var arrPuntos = [];
        for(var k in arrayLineas){
          for (var j = 0; j<ruta.length - 1; j++){
            if (arrayLineas[k]['locInicio'] == ruta[j] && arrayLineas[k]['locFin'] == ruta[j+1] || arrayLineas[k]['locInicio'] == ruta[j+1] && arrayLineas[k]['locFin'] == ruta[j]){
              arrayLineas[k].set({fill:'blue', stroke: 'blue'});
              arrPuntos.push([arrayLineas[k].x1, arrayLineas[k].y1]);
              arrPuntos.push([arrayLineas[k].x2, arrayLineas[k].y2]);
            }else{
              if (localidadInicio != "" || localidadFin != ""){
                if (cont < 1){
                  if(arrayLineas[k]['locInicio'] == localidadInicio && arrayLineas[k]['locFin'] == localidadFin || arrayLineas[k]['locInicio'] == localidadFin && arrayLineas[k]['locFin'] == localidadInicio ){
                    zoomw = (arrayLineas[k]['x1']+arrayLineas[k]['x2'])/2;
                    zoomh = (arrayLineas[k]['y1']+arrayLineas[k]['y2'])/2;
                    arrayLineas[k].set({fill:'green', stroke: 'green'});
                    arrPuntos.push([arrayLineas[k].x1, arrayLineas[k].y1]);
                    arrPuntos.push([arrayLineas[k].x2, arrayLineas[k].y2]);
                    cont++;
                  }
                }
              }
              
            }
            
          }
        }

        arrPuntos = arrPuntos.filter((t = {}, a=> !(t[a]=a in t)));
        var puntoMedio = center(arrPuntos);
        for (var m = 0; m < ruta.length; m++){
          for (var n in listaCirc){
            if (listaCirc[n]['name'] == ruta[m]){
              listaCirc[n].set({stroke: '#dfe86d'});
            }
          }
        }

        //canvas.zoomToPoint(new fabric.Point(zoomw,zoomh),1);
        //console.log(puntoMedio);
        
        var centerp = canvas.getCenter();
        //canvas.absolutePan(new fabric.Point(-100, -100));
        canvas.absolutePan(new fabric.Point(centerp.left, centerp.top));
        canvas.absolutePan(new fabric.Point(puntoMedio[0] - centerp.left + 300, puntoMedio[1] - centerp.top - (puntoMedio[1] - centerp.top > 0 ? (puntoMedio[1] - centerp.top > 300 ? 200 : 0) : -100)));
        canvas.renderAll();
      });
    }

    function getCircuito (circuito){
      circuitosEncontrados = [];
      for (let i in dataJSON.CRS){
        if (dataJSON.CRS[i].codCircuito.indexOf(circuito) >= 0){
          circuitosEncontrados.push(dataJSON.CRS[i]);
        }
      }
      circuitosEncontrados.sort((a,b) => {
        return a.orden - b.orden;
      });

      circuitosEncontrados.reverse();

        if(circuitosEncontrados.length){
          let info = ``;
          var circuito = [];
          for(let j in circuitosEncontrados){
            if (circuitosEncontrados[j]['BW'] > 4300){
              info += `<tr class='table-success' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            } else if (circuitosEncontrados[j]['BW'] > 1100){
              info += `<tr class='table-warning' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            } else {
              info += `<tr class='table-danger' ${j > 4 ? 'style="display:none"' : ''}><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td>${circuitosEncontrados[j]['BW']}</td><td>${circuitosEncontrados[j]['latencia']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;
  
            }
            circuito.push(circuitosEncontrados[j]['circuito']);
          }
          let num;
          n = circuitosEncontrados.length;
          if (circuitosEncontrados.length % 5 != 0){
            num = Math.floor(circuitosEncontrados.length /5) + 1;
          }else {
            num = circuitosEncontrados.length / 5;
          }

          let pagin = ``;
          
          for (let i = 0; i < num; i++){
            pagin += `<li class='navpage page-item ${ i == 0 ? 'active' : ''}'><a class='page-link clickPage' href='#'>${ i+1 }</a></li>`;
          }
          $('#cuerpoTabla').empty();
          $('.navpage').remove();
          $('#contenido').hide('fast');
          $('#cuerpoTabla').append(info);
          $('#paginat > li:nth-child(1)').after(pagin);
          
          
          $('a.clickPage').click((e)=> {
            $('#paginat > li.active').toggleClass('active');
            $(e.target).parent().addClass('active');
            var numItem = $(e.target).text();
            for (let i = 1; i <= circuitosEncontrados.length; i++){
              let $row = $(`#cuerpoTabla > tr:nth-child(${i})`);
              if (i > (numItem-1)*5 && i <= numItem*5){
                $row.show();
              }else{
                $row.hide();
              }
              
            }
            
            return false;
          });
          $('#infoAdicional').hide();
          $('#infoTramo').html(`<b>Resultados de la Búsqueda:</b>`);
          $('#infoTramo').show('fast');
          $('#infoTabla').show('fast');
          $('#paginationDiv').show('fast');
  
          for(var j in circuitosEncontrados){
            setEvent(circuitosEncontrados[j]['circuito'], j, "", "");
          }
  
        }else{
          $('#infoTramo').hide();
          $('#infoTabla').hide();
          $('#infoAdicional').html("<div class='alert alert-danger' role='alert'>No se han encontrado coincidencias en la búsqueda.</div>");
          $('#infoAdicional').show('fast');
        }
    }
  })();

  

  function center(arr){
    var minX, maxX, minY, maxY;
    for (var i = 0; i < arr.length; i++)
    {
        minX = (arr[i][0] < minX || minX == null) ? arr[i][0] : minX;
        maxX = (arr[i][0] > maxX || maxX == null) ? arr[i][0] : maxX;
        minY = (arr[i][1] < minY || minY == null) ? arr[i][1] : minY;
        maxY = (arr[i][1] > maxY || maxY == null) ? arr[i][1] : maxY;
    }
    return [(minX + maxX) / 2, (minY + maxY) / 2];
  }

  $('#pageAnterior').click((e)=> {
    if (!$('#paginat > li.active').prev().is('li:first')){
      let actual = $('#paginat > li.active');
      actual.toggleClass('active');
      let prev = actual.prev();
      prev.toggleClass('active');
      let numItem = prev.text();
      for (let i = 1; i <= n; i++){
        let $row = $(`#cuerpoTabla > tr:nth-child(${i})`);
        if (i > (numItem-1)*5 && i <= numItem*5){
          $row.show();
        }else{
          $row.hide();
        }
      }
    }
    return false;
  });

  $('#pageSiguiente').click((e)=> {
    if (!$('#paginat > li.active').next().is('li:last')){
      let actual = $('#paginat > li.active');
      actual.toggleClass('active');
      let next = actual.next();
      let numItem = next.text();
      next.toggleClass('active');
      for (let i = 1; i <= n; i++){
        let $row = $(`#cuerpoTabla > tr:nth-child(${i})`);
        if (i > (numItem-1)*5 && i <= numItem*5){
          $row.show();
        }else{
          $row.hide();
        }
      }
    }
    return false;
  });