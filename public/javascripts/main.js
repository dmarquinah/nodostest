var points=[];
var circuito = [];
var listaCirc = [];
var dataJSON;
(function() {
    var canvas = this.__canvas = new fabric.Canvas('c', { selection: false });
    canvas.setHeight(window.innerHeight);
    canvas.setWidth($('#canvasDiv').width());
    canvas.setZoom(1);
    fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
  
    function makeCircle(left, top, name, arrayLineasAsociadas) {
      // console.log(arrayLineasAsociadas);
      var c = new fabric.Circle({
        left: left,
        top: top,
        strokeWidth: 3,
        radius: 6,
        fill: '#fff',
        stroke: '#666',
        name: name
      });
      c.hasControls = c.hasBorders = false;
      for (var i in arrayLineasAsociadas){
        c[`line${i}`] = arrayLineasAsociadas[i];
      }
      return c;
    }

    function makeText(left, top, text){
      if(text.indexOf(' ')>=0){
        text = text.replace(' ','\n');
      }
      var t = new fabric.Text(text, {
        fontSize: 10,
        left: left,
        top: top-15,
        fontFamily: 'Verdana',
        selectable: false,
        evented: false
      });
      return t;
    }
  
    function makeLine(coords, locInicio, locFin) {
      return new fabric.Line(coords, {
        fill: 'red',
        stroke: 'red',
        strokeWidth: 3,
        selectable: false,
        evented: true,
        locInicio: locInicio,
        locFin: locFin
      });
    }
    
    var panning = false;
    canvas.on('mouse:up', function(e){
        // if(e.target != null){
        //   points.push([e.target.name, e.target.left, e.target.top]);
        // }
        panning = false;
        if (e.target){
          console.log(e.target);
          var name = e.target.name;
          for (var localidad in dataJSON['localidades']){
            if(dataJSON['localidades'][localidad][0] == name){
              dataJSON['localidades'][localidad][1] = Math.trunc(e.target.left);
              dataJSON['localidades'][localidad][2] = Math.trunc(e.target.top);
              break;
            }
          }
          for (var circuito in dataJSON['circuitos']){
            for (var loc in dataJSON['circuitos'][circuito]['circuito']){
              if (dataJSON['circuitos'][circuito]['circuito'][loc] == name){
                dataJSON['circuitos'][circuito]['locCircuitos'][loc][0] = Math.trunc(e.target.left);
                dataJSON['circuitos'][circuito]['locCircuitos'][loc][1] = Math.trunc(e.target.top);
              }
            }
          }
        }
        

    });

    canvas.on('mouse:down', function(e){
      if (e.target == null){
        panning = true;
      }else{
        //console.log(e.target);
        getRoutesTable(e.target.locInicio, e.target.locFin);
      }
  });   
    canvas.on('mouse:move', function(e){
      if(panning && e && e.e){
        var delta =  new fabric.Point(e.e.movementX, e.e.movementY);
        canvas.relativePan(delta);
      }
    }); 


    canvas.on('mouse:wheel', function(opt) {
      var delta = opt.e.deltaY;
      var pointer = canvas.getPointer(opt.e);
      
      var zoom = canvas.getZoom();
      zoom = zoom - delta/200;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.5) zoom = 0.5;
      canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
      opt.e.preventDefault();
      opt.e.stopPropagation();
    });

    canvas.on('object:moving', function(opt){
      var p = opt.target;
      var objProp = Object.keys(p);
      for(var i in objProp){
        if(objProp[i].indexOf('line') >= 0){
          if(p.name == p[objProp[i]]['locInicio']){
            p[objProp[i]] && p[objProp[i]].set({'x1':p.left, 'y1':p.top});
          }else if(p.name == p[objProp[i]]['locFin']){
            p[objProp[i]] && p[objProp[i]].set({'x2':p.left, 'y2':p.top});
          }
        }
      }
      canvas.renderAll();
    });

    var arrayLineas = [];
    var arrayEnlaces = [];
    jQuery.get('/javascripts/data2.JSON',(data)=> {   
      dataJSON = data;   
      var circ;
      var circNombre;
      for(var i in data.circuitos){
        circNombre = data.circuitos[i]['circuito'];
        circ = data.circuitos[i]['locCircuitos'];
        for (var l = 0; l<circ.length-1; l++){
          crearLineas(circ[l], circ[l+1], circNombre[l], circNombre[l+1]);
        }
      }
      //En arrayLineas se guardan todas las lineas (conexiones) existentes
      var lineaRef;
      for (var i in data.localidades){
        //cada localidad (circulo) tendrá ciertas líneas asignadas
        lineaRef = [];
        for (var j in arrayLineas){
          if (arrayLineas[j]['locInicio'] == data.localidades[i][0] || arrayLineas[j]['locFin'] == data.localidades[i][0]){
            lineaRef.push(arrayLineas[j]);  
          }
        }
        // console.log(lineaRef);
        crearCirculos(data.localidades[i], lineaRef);
      }
      canvas.renderAll();
      // var ci;
      // for (var i in data.circuitos){
      //   ci = [];
      //   for (var j in data.circuitos[i]['circuito']){
      //     for (var k in data.localidades){
      //       if (data.circuitos[i]['circuito'][j] == data.localidades[k][0]){
      //         ci.push([data.localidades[k][1],data.localidades[k][2]]);
      //       }
      //     }
      //   }
      //   data.circuitos[i]['locCircuitos'] = ci;
      // }
      // console.log(JSON.stringify(data));


    });

    function crearCirculos(dataArray ,lineasAsociadas){
      canvas.add(makeCircle(dataArray[1],dataArray[2], dataArray[0],lineasAsociadas));
      canvas.add(makeText(dataArray[1],dataArray[2], dataArray[0]));
    }
    function crearLineas(arrayInicio, arrayFin, localidadInicio, localidadFinal){
      var len = arrayEnlaces.length;
      arrayEnlaces.push(`${localidadInicio}-${localidadFinal}`);
      arrayEnlaces = [...new Set(arrayEnlaces)];
      var linea = makeLine([arrayInicio[0],arrayInicio[1],arrayFin[0],arrayFin[1]], localidadInicio, localidadFinal);
      if(len != arrayEnlaces.length){
        arrayLineas.push(linea);
        canvas.add(linea);
      } 
    }
    function getRoutesTable(locInicio, locFin){
      var circuitosEncontrados = [];
      var indexInicio;
      var indexFin;
      for(var i in dataJSON.circuitos){
        var indexInicio = dataJSON['circuitos'][i]['circuito'].indexOf(locInicio);
        if( indexInicio >= 0){
          indexFin = dataJSON['circuitos'][i]['circuito'].indexOf(locFin);
          if( indexFin >= 0 && Math.abs(indexFin - indexInicio) == 1){
            circuitosEncontrados.push(dataJSON.circuitos[i]);
          }
        }
      }
      
      circuitosEncontrados.sort((a,b) => {
        return a.orden - b.orden;
      });

      if(circuitosEncontrados.length){
        var info = `<table class='table table-bordered table-hover text-center table-sm'><thead class='thead-light'><tr><th scope='col'>#</th><th scope='col'>cod. Circuito</th><th scope='col'>Circuito</th><th scope='col'>Ruta</th></thead><tbody>`;
        var circuito = [];
        for(var j in circuitosEncontrados){
          if (circuitosEncontrados[j]['orden'] < 6){
            info += `<tr class='table-success'><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          } else if (circuitosEncontrados[j]['orden'] < 20){
            info += `<tr class='table-warning'><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          } else {
            info += `<tr class='table-danger'><th scope='row'>${j}</th><td>${circuitosEncontrados[j]['codCircuito']}</td><td>${circuitosEncontrados[j]['nombreCircuito']}</td><td><button class='btn btn-secondary' id='ruta${j}'>Ver</button></td></tr>`;

          }
          circuito.push(circuitosEncontrados[j]['circuito']);
        }
        info += '</tbody></table>';
    
        $('#infoTabla').html(info);
        $('#infoTramo').html(`<b>TRAMO ${locInicio} - ${locFin}</b>`);
        $('#infoTramo').show();

        for(var j in circuitosEncontrados){
          setEvent(circuitosEncontrados[j]['circuito'], j, locInicio, locFin);
        }

      }else{
        $('#infoTabla').html("<h2>No se han encontrado coincidencias</h2>");
      }
      
      function setEvent(ruta, i, localidadInicio, localidadFin){
        $("#ruta" + i).on('click', function(e){
          // var zoomh;
          // var zoomw;
          for (var l in arrayLineas){
            arrayLineas[l].set({fill:'red', stroke: 'red'});
          }
          
          for (var j = 0; j<ruta.length - 1; j++){
            for(var k in arrayLineas){
              
              if (arrayLineas[k]['locInicio'] == ruta[j] && arrayLineas[k]['locFin'] == ruta[j+1] || arrayLineas[k]['locInicio'] == ruta[j+1] && arrayLineas[k]['locFin'] == ruta[j]){
                arrayLineas[k].set({fill:'blue', stroke: 'blue'});
              }
              if(arrayLineas[k]['locInicio'] == localidadInicio && arrayLineas[k]['locFin'] == localidadFin || arrayLineas[k]['locInicio'] == localidadFin && arrayLineas[k]['locFin'] == localidadInicio ){
                //zoomw = (arrayLineas[k]['x1']+arrayLineas[k]['x2'])/2;
                //zoomh = (arrayLineas[k]['y1']+arrayLineas[k]['y2'])/2;
                arrayLineas[k].set({fill:'green', stroke: 'green'});
              }
            }
          }
          // console.log(zoomw,zoomh);
          // canvas.zoomToPoint(new fabric.Point(zoomw,zoomh),1);
          canvas.renderAll();
        });
      }
    
    }
  })();